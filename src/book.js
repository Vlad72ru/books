// @flow 
import * as React from 'react';


const Book = ({img, title, author}) => {

    const clickHandler = () => {
        alert('random message')
    }

    const complexExample = (author) => {
        alert(`Passing the author's name ${author}`)
    }

    const activeElementBg = () => {
        console.log(`you hovered over an element`)
    }

    return <article className='book' onMouseOver={() =>activeElementBg()}>
        <img src={img} alt=""/>
        <h1>{title}</h1>
        <h4>{author}</h4>
        <button type="button" onClick={clickHandler}>reference example</button>
        <button type="button" onClick={() => complexExample(author)}>more complex example</button>
    </article>
}
export default Book