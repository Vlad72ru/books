import React from "react";
import ReactDom from 'react-dom'

//css
import './index.css';

import {data} from "./books";
import SpecificBook from "./book";

const BooksList = () => {
    return(
        <section className="book-list">
            {data.map((book) => {
                return <SpecificBook key={book.id} {...book}></SpecificBook>
            })}
        </section>
    )
}



ReactDom.render(<BooksList/>, document.getElementById('root'))