export const data = [
    {
        id: 1,
        img : 'https://images-na.ssl-images-amazon.com/images/I/911V88A7KeL._AC_UL210_SR195,210_.jpg',
        title : 'The Paris Apartment: A Novel',
        author : 'Author Name',
    },
    {
        id: 2,
        img : 'https://images-na.ssl-images-amazon.com/images/I/91+f5eBUlEL._AC_UL210_SR195,210_.jpg',
        title : 'The Hobbit',
        author : 'Author Name 2',
    },
    {
        id: 3,
        img : 'https://images-na.ssl-images-amazon.com/images/I/911cmGSgcvL._AC_UL210_SR195,210_.jpg',
        title : 'Will',
        author : 'Author Name 3',
    }
]